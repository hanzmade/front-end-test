import Image from 'next/image'
import { faHome,faBook, faChartBar,faCommentDots,faBell,faUser,faSearch,faEllipsisV } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
export default function Header(){
    return(
        <div className="bg-white shadow-md overflow-hidden w-screen mb-5">
            <div className="ml-5 grid grid-flow-col mt-1">
                <Image src="/images/studext-logo.svg" alt="Studext" width={30} height={50} />
                <div className="input-group ml-10 md:mr-10 ">
                    <input type="text" className="bg-gray-100 rounded-full border-none" placeholder="Search..."/>
                    <FontAwesomeIcon className="icon-input" width="20" icon={faSearch} />
                </div> 
                <a href="#" className="text-xs text-gray-500 grid grid-flow-col mt-2 mr-10 d-none-menu"><FontAwesomeIcon icon={faHome} width="25" /> Home</a>
                <a href="#" className="text-xs text-gray-500 grid grid-flow-col mt-2 mr-10 d-none-menu"><FontAwesomeIcon icon={faBook} width="20" /> My Course</a>
                <a href="#" className="text-xs text-blue-500 grid grid-flow-col mt-2 mr-10 d-none-menu"><FontAwesomeIcon icon={faChartBar} width="20" /> Dashboard</a>
                <a href="#" className="text-xs text-gray-500 grid grid-flow-col mt-2 mr-10 d-none-menu"><FontAwesomeIcon icon={faCommentDots} width="20" /> Chat</a>
                <a href="#" className="text-xs text-gray-500 grid grid-flow-col mt-2 mr-10 d-none-menu"><FontAwesomeIcon icon={faBell} width="20" /> Notification</a>
                <a href="#" className="text-xs text-gray-500 grid grid-flow-col mt-2 mr-10 d-none-menu"><FontAwesomeIcon icon={faUser} width="20" /> Profile</a> 
                <a href="#" className="text-xs text-gray-500 grid grid-flow-col mt-4 md:hidden ml-5 mr-2"><FontAwesomeIcon icon={faEllipsisV} width="8" /></a> 
            </div>
        </div>
    )
}