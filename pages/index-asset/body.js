import Header from '../../layout/header.js'
import Image from 'next/image'
import Story from './contents/story.js'
import { faStar,faStarHalf,faDownload,faUser,faChevronRight } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Swiper, SwiperSlide } from 'swiper/react';

export default function Home() {
  // Statistic Chart
  let React = require('react'),
  ReactDOM = require('react-dom'),
  Highcharts = require('highcharts'),
  addFunnel = require('highcharts/modules/funnel');
  let element = React.createElement('div', { id: 'chart' });
  if (typeof window === 'object') {
    ReactDOM.render(element, document.getElementById('bar-charts'), function () {
      addFunnel(Highcharts);
      Highcharts.chart('chart', {
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['1 Jan', '2 Jan', '3 Jan', '4 Jan', '5 Jan', '6 Jan', '7 Jan', '8 Jan', '9 Jan', '10 Jan', '11 Jan', '12 Jan', '13 Jan', '14 Jan', '15 Jan', '16 Jan', '17 Jan']
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: [{
            name: 'Up',
            data: [5, 3, 10, 7, 2,5, 3, 10, 7, 2, 5, 3, 10, 7, 2, 5, 3],
            color: '#008FFF'
        }, {
            name: 'Down',
            data: [-2, -2, -3, -6, -1, -5, -3, -10, -7, -2, -5, -3, -15, -7, -2, -5, -3],
            color: '#DB0038'
        }]
      });
    });
  }
  // End Statistic Chart

  return (
      <main className="container">
        <Header></Header>
        <div className="md:grid md:grid-cols-4 grid-flow-col gap-0">
          <div className="col-span-3">
            <div className="grid grid-cols-4 grid-flow-col gap-4 mx-5">
              <div className="mx-auto bg-white shadow-md overflow-hidden w-full col-span-4 rounded-3xl">
                <label className="mx-10 font-serif font-bold">Story</label>
                <Story></Story>
              </div>
            </div>
            <div className="grid grid-cols-4 grid-flow-col gap-4 mx-5 mt-5">
              <div className="mx-auto bg-white shadow-md overflow-hidden w-full col-span-4 rounded-3xl">
                <div className="grid grid-cols-4">
                  <label className="mx-10 font-serif font-bold col-span-3 pt-10">Statistic</label>
                  <div className="relative inline-block text-left mt-10 mx-10 float-right">
                    <div>
                      <button type="button" className="inline-flex justify-center w-full rounded-full border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500" id="menu-button" aria-expanded="true" aria-haspopup="true">
                        Month
                        <svg className="-mr-1 ml-2 h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                          <path fillRule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clipRule="evenodd" />
                        </svg>
                      </button>
                    </div>
                  </div>
                </div>
                <div>
                  <figure className="highcharts-figure">
                      <div id="bar-charts"></div>
                  </figure>
                </div>
              </div>
            </div>
          </div>
          <div className="col-span-1 mt-5-custom">
            <div className="grid grid-cols-4 grid-flow-col mx-5">
              <div className="p-2 mx-auto bg-white shadow-md overflow-hidden w-full col-span-4 rounded-3xl">
                <label className="ml-5 font-serif font-bold">Peringkat</label>
                <div className="flex float-right">
                  <Image alt="logo" className="" width={75} height={75} src="/images/trophy.png"/>
                </div>
                <div className="ml-5 text-xs text-2xs-custom">
                  Hasil Akhir Perolehan Nilai Tryout
                </div>
                <div className="ml-5 mt-2">
                  <a href="#"><Image alt="logo" className="" width={16} height={16} src="/images/download.svg"/> <span className="text-xs text-blue-download-csv">Unduh .CSV</span></a>
                </div>

                <Swiper
                    spaceBetween={10}
                    slidesPerView={5}
                    onSlideChange={() => console.log('slide change')}
                    onSwiper={(swiper) => console.log(swiper)}
                    className="w-full"
                    direction={'vertical'}
                >
                  <SwiperSlide className="">
                    <div className="bg-gradient-to-r from-yellow-400 to-yellow-100 p-3 shadow-md rounded-xl w-full ml-2 mr-2">
                      <span className="float-left mr-2 mt-3 text-xs px-2 bg-white text-black rounded-full">1</span>
                      <Image alt="user" className="rounded-full" width={30} height={30} src="/images/me.jpg"/>
                      <label className="absolute ml-2 mt-3 text-xs truncate">Hanif Ram...</label>
                      <span className="absolute ml-24 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-blue-400 rounded-full">40</span>
                      <span className="absolute ml-28 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-red-400 rounded-full">0</span>
                      <span className="absolute ml-32 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-black rounded-full">0</span>
                      <span className="absolute ml-36 mt-3 text-xs text-xs-custom px-1 text-black rounded-full font-bold">100</span>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide className="">
                    <div className="bg-gradient-to-r from-gray-400 to-gray-100 p-3 shadow-md rounded-xl w-full ml-2 mr-2">
                    <span className="float-left mr-2 mt-3 text-xs px-2 bg-white text-black rounded-full">2</span>
                      <Image alt="user" className="rounded-full" width={30} height={30} src="/images/me.jpg"/>
                      <label className="absolute ml-2 mt-3 text-xs truncate">Hanif Ram...</label>
                      <span className="absolute ml-24 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-blue-400 rounded-full">39</span>
                      <span className="absolute ml-28 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-red-400 rounded-full">1</span>
                      <span className="absolute ml-32 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-black rounded-full">0</span>
                      <span className="absolute ml-36 mt-3 text-xs text-xs-custom px-1 text-black rounded-full font-bold">98</span>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide className="">
                    <div className="bg-gradient-to-r from-yellow-700 to-yellow-50 p-3 shadow-md rounded-xl w-full ml-2 mr-2">
                    <span className="float-left mr-2 mt-3 text-xs px-2 bg-white text-black rounded-full">3</span>
                      <Image alt="user" className="rounded-full" width={30} height={30} src="/images/me.jpg"/>
                      <label className="absolute ml-2 mt-3 text-xs truncate">Hanif Ram...</label>
                      <span className="absolute ml-24 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-blue-400 rounded-full">35</span>
                      <span className="absolute ml-28 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-red-400 rounded-full">3</span>
                      <span className="absolute ml-32 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-black rounded-full">2</span>
                      <span className="absolute ml-36 mt-3 text-xs text-xs-custom px-1 text-black rounded-full font-bold">90</span>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide className="">
                    <div className="bg-gray-100 p-3 shadow-md rounded-xl w-full ml-2 mr-2">
                    <span className="float-left mr-2 mt-3 text-xs px-2 bg-white text-black rounded-full">4</span>
                      <Image alt="user" className="rounded-full" width={30} height={30} src="/images/me.jpg"/>
                      <label className="absolute ml-2 mt-3 text-xs truncate">Hanif Ram...</label>
                      <span className="absolute ml-24 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-blue-400 rounded-full">30</span>
                      <span className="absolute ml-28 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-red-400 rounded-full">5</span>
                      <span className="absolute ml-32 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-black rounded-full">5</span>
                      <span className="absolute ml-36 mt-3 text-xs text-xs-custom px-1 text-black rounded-full font-bold">80</span>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide className="">
                    <div className="bg-gray-100 p-3 shadow-md rounded-xl w-full ml-2 mr-2">
                    <span className="float-left mr-2 mt-3 text-xs px-2 bg-white text-black rounded-full">5</span>
                      <Image alt="user" className="rounded-full" width={30} height={30} src="/images/me.jpg"/>
                      <label className="absolute ml-2 mt-3 text-xs truncate">Hanif Ram...</label>
                      <span className="absolute ml-24 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-blue-400 rounded-full">29</span>
                      <span className="absolute ml-28 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-red-400 rounded-full">11</span>
                      <span className="absolute ml-32 mt-3 text-xs text-3xs-custom px-1 bg-gray-50 text-black rounded-full">0</span>
                      <span className="absolute ml-36 mt-3 text-xs text-xs-custom px-1 text-black rounded-full font-bold">78</span>
                    </div>
                  </SwiperSlide>
                </Swiper>
              </div>
            </div>
          </div>
        </div>

        <div className="grid grid-cols-4 grid-flow-col mx-5 mt-5 mb-5 inline-block">
          <div className="mx-auto bg-white shadow-md overflow-hidden w-full col-span-4 rounded-3xl">
            <label className="mx-10 font-serif font-bold">Course Preview</label>
            <Swiper
                spaceBetween={300}
                slidesPerView={4}
                onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}
            >
              <SwiperSlide className="mx-5 mr-96">
                <div className="mt-5 bg-white shadow-md w-80 rounded-3xl mx-5 my-5">
                    <div className="bg-white mx-5 grid grid-cols-2-custom">
                      <div className="w-max">
                        <Image alt="user" className="rounded-3xl" width={100} height={100} src="/images/me.jpg"/>
                      </div>
                      <div className="grid-cols-2-custom pl-20 inline-block">
                        <div className="flex">
                          <label className="">TOEFL</label>
                          <span className="ml-20 "><FontAwesomeIcon color="#008FFF" icon={faChevronRight} width="15" /></span>
                        </div>
                        <div className="flex">
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <label className="text-xs pl-2">(20)</label>
                        </div>
                        <div className="flex w-max mt-2">
                          <Image alt="user" className="" width={20} height={20} src="/images/logo-course-hero.png"/>
                          <label className="text-xs text-2xs-custom truncate ml-2 align-middle">By Course Hero</label>
                        </div>
                        <div className="mt-4 flex w-max">  
                          <span className=""><FontAwesomeIcon className="" color="#ADADAD" icon={faUser} width="15" /></span>
                          <label className="ml-3 text-xs text-2xs-custom">203 Pelajar, 4 Guru</label>
                        </div>
                      </div>
                      <div className="w-max my-5">
                        <span className="text-gray-400 float-right line-through">Rp. 2.000.000</span>
                        <div>
                          <span className="text-xs p-1 bg-red-200 text-red-500 rounded-full truncate">Mulai:<span className="font-bold">17 Agustus 2021</span></span>
                          <span className="text-blue-400 ml-10">Rp. 1.500.000</span>
                        </div>
                      </div>
                    </div>
                </div>
              </SwiperSlide>
              <SwiperSlide className="mx-2 mr-96">
                <div className="mt-5 bg-white shadow-md w-80 rounded-3xl mx-5 my-5">
                    <div className="bg-white mx-5 grid grid-cols-2-custom">
                      <div className="w-max">
                        <Image alt="user" className="rounded-3xl" width={100} height={100} src="/images/me.jpg"/>
                      </div>
                      <div className="grid-cols-2-custom pl-20 inline-block">
                        <div className="flex">
                          <label className="">TOEFL</label>
                          <span className="ml-20 "><FontAwesomeIcon color="#008FFF" icon={faChevronRight} width="15" /></span>
                        </div>
                        <div className="flex">
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <label className="text-xs pl-2">(20)</label>
                        </div>
                        <div className="flex w-max mt-2">
                          <Image alt="user" className="" width={20} height={20} src="/images/logo-course-hero.png"/>
                          <label className="text-xs text-2xs-custom truncate ml-2 align-middle">By Course Hero</label>
                        </div>
                        <div className="mt-4 flex w-max">  
                          <span className=""><FontAwesomeIcon className="" color="#ADADAD" icon={faUser} width="15" /></span>
                          <label className="ml-3 text-xs text-2xs-custom">203 Pelajar, 4 Guru</label>
                        </div>
                      </div>
                      <div className="w-max my-5">
                        <span className="text-gray-400 float-right line-through">Rp. 2.000.000</span>
                        <div>
                          <span className="text-xs p-1 bg-red-200 text-red-500 rounded-full truncate">Mulai:<span className="font-bold">17 Agustus 2021</span></span>
                          <span className="text-blue-400 ml-10">Rp. 1.500.000</span>
                        </div>
                      </div>
                    </div>
                </div>
              </SwiperSlide>
              <SwiperSlide className="mx-2 mr-96">
                <div className="mt-5 bg-white shadow-md w-80 rounded-3xl mx-5 my-5">
                    <div className="bg-white mx-5 grid grid-cols-2-custom">
                      <div className="w-max">
                        <Image alt="user" className="rounded-3xl" width={100} height={100} src="/images/me.jpg"/>
                      </div>
                      <div className="grid-cols-2-custom pl-20 inline-block">
                        <div className="flex">
                          <label className="">TOEFL</label>
                          <span className="ml-20 "><FontAwesomeIcon color="#008FFF" icon={faChevronRight} width="15" /></span>
                        </div>
                        <div className="flex">
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <label className="text-xs pl-2">(20)</label>
                        </div>
                        <div className="flex w-max mt-2">
                          <Image alt="user" className="" width={20} height={20} src="/images/logo-course-hero.png"/>
                          <label className="text-xs text-2xs-custom truncate ml-2 align-middle">By Course Hero</label>
                        </div>
                        <div className="mt-4 flex w-max">  
                          <span className=""><FontAwesomeIcon className="" color="#ADADAD" icon={faUser} width="15" /></span>
                          <label className="ml-3 text-xs text-2xs-custom">203 Pelajar, 4 Guru</label>
                        </div>
                      </div>
                      <div className="w-max my-5">
                        <span className="text-gray-400 float-right line-through">Rp. 2.000.000</span>
                        <div>
                          <span className="text-xs p-1 bg-red-200 text-red-500 rounded-full truncate">Mulai:<span className="font-bold">17 Agustus 2021</span></span>
                          <span className="text-blue-400 ml-10">Rp. 1.500.000</span>
                        </div>
                      </div>
                    </div>
                </div>
              </SwiperSlide>
              <SwiperSlide className="mx-2 mr-96">
                <div className="mt-5 bg-white shadow-md w-80 rounded-3xl mx-5 my-5">
                    <div className="bg-white mx-5 grid grid-cols-2-custom">
                      <div className="w-max">
                        <Image alt="user" className="rounded-3xl" width={100} height={100} src="/images/me.jpg"/>
                      </div>
                      <div className="grid-cols-2-custom pl-20 inline-block">
                        <div className="flex">
                          <label className="">TOEFL</label>
                          <span className="ml-20 "><FontAwesomeIcon color="#008FFF" icon={faChevronRight} width="15" /></span>
                        </div>
                        <div className="flex">
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <label className="text-xs pl-2">(20)</label>
                        </div>
                        <div className="flex w-max mt-2">
                          <Image alt="user" className="" width={20} height={20} src="/images/logo-course-hero.png"/>
                          <label className="text-xs text-2xs-custom truncate ml-2 align-middle">By Course Hero</label>
                        </div>
                        <div className="mt-4 flex w-max">  
                          <span className=""><FontAwesomeIcon className="" color="#ADADAD" icon={faUser} width="15" /></span>
                          <label className="ml-3 text-xs text-2xs-custom">203 Pelajar, 4 Guru</label>
                        </div>
                      </div>
                      <div className="w-max my-5">
                        <span className="text-gray-400 float-right line-through">Rp. 2.000.000</span>
                        <div>
                          <span className="text-xs p-1 bg-red-200 text-red-500 rounded-full truncate">Mulai:<span className="font-bold">17 Agustus 2021</span></span>
                          <span className="text-blue-400 ml-10">Rp. 1.500.000</span>
                        </div>
                      </div>
                    </div>
                </div>
              </SwiperSlide>
              <SwiperSlide className="mx-2 mr-96">
                <div className="mt-5 bg-white shadow-md w-80 rounded-3xl mx-5 my-5">
                    <div className="bg-white mx-5 grid grid-cols-2-custom">
                      <div className="w-max">
                        <Image alt="user" className="rounded-3xl" width={100} height={100} src="/images/me.jpg"/>
                      </div>
                      <div className="grid-cols-2-custom pl-20 inline-block">
                        <div className="flex">
                          <label className="">TOEFL</label>
                          <span className="ml-20 "><FontAwesomeIcon color="#008FFF" icon={faChevronRight} width="15" /></span>
                        </div>
                        <div className="flex">
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <span><FontAwesomeIcon color="#F7C200" icon={faStar} width="15" /></span>
                          <label className="text-xs pl-2">(20)</label>
                        </div>
                        <div className="flex w-max mt-2">
                          <Image alt="user" className="" width={20} height={20} src="/images/logo-course-hero.png"/>
                          <label className="text-xs text-2xs-custom truncate ml-2 align-middle">By Course Hero</label>
                        </div>
                        <div className="mt-4 flex w-max">  
                          <span className=""><FontAwesomeIcon className="" color="#ADADAD" icon={faUser} width="15" /></span>
                          <label className="ml-3 text-xs text-2xs-custom">203 Pelajar, 4 Guru</label>
                        </div>
                      </div>
                      <div className="w-max my-5">
                        <span className="text-gray-400 float-right line-through">Rp. 2.000.000</span>
                        <div>
                          <span className="text-xs p-1 bg-red-200 text-red-500 rounded-full truncate">Mulai:<span className="font-bold">17 Agustus 2021</span></span>
                          <span className="text-blue-400 ml-10">Rp. 1.500.000</span>
                        </div>
                      </div>
                    </div>
                </div>
              </SwiperSlide>
            </Swiper>
          </div>
        </div>
      </main>
  )
}
