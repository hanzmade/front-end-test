import Image from 'next/image'
import { Swiper, SwiperSlide } from 'swiper/react';

export default function Story() {
    return(
    
        <Swiper
            spaceBetween={60}
            slidesPerView={8}
            onSlideChange={() => console.log('slide change')}
            onSwiper={(swiper) => console.log(swiper)}
        >
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            <SwiperSlide className="mx-5">
            <ul className="flex w-20">
                <li>
                    <div className="p-1 rounded-full bg-gradient-to-tr from-blue-900 to-blue-300">
                        <a className="block pb-0 bg-white p-1 rounded-full transform hover:-rotate-6">
                            <Image alt="user" className="rounded-full" width={70} height={70} src="/images/me.jpg"/>
                        </a>
                    </div>
                    <label className="text-xs font-serif">Hanif Ram...</label>
                </li>
                
            </ul>
            </SwiperSlide>
            
        </Swiper>
    )
}