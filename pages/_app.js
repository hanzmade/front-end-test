import '../assets/css/tailwind.css'
import '../assets/css/customize.css'
import 'swiper/swiper.scss';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
