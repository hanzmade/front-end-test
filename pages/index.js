import Head from 'next/head'
import Image from 'next/image'
import Body from './index-asset/body.js'

export default function Home() {
  return (
    <div className="container">
      <Head>
        <title>Front End Test</title>
        <meta name="description" content="Front End Test" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Body></Body>

      {/* <footer className="container mt-20">
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <span className="text-center">
            <Image src="/vercel.svg" alt="Vercel Logo" width={72} height={16} />
          </span>
        </a>
      </footer> */}
    </div>
  )
}
